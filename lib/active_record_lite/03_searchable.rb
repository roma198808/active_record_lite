require_relative 'db_connection'
require_relative '02_sql_object'

module Searchable
  def where(params)
    table_name = "#{self}s"
    where_line = params.map { |key,val| "#{key} = ?" }.join(" AND ")
    values = params.map { |k,v| v }

    result = DBConnection.execute(<<-SQL, values)
    SELECT *
    FROM #{table_name}
    WHERE #{where_line}
    SQL

    result.map { |el| self.new(el) }
  end
end

class SQLObject
  extend Searchable
end
