require_relative '00_attr_accessor_object.rb'

class MassObject < AttrAccessorObject
  def self.my_attr_accessible(*new_attributes)
    new_attributes.each do |el|
      self.attributes << el
    end
  end

  def self.attributes
    if self == MassObject
      raise "must not call #attributes on MassObject directly"
    end
    @attributes ||= []
  end

  def initialize(params = {})
    params.each do |key, value|
      if self.class.attributes.include?(key.to_sym)
        self.send("#{key}=".to_sym, value)
      else
        raise "mass assignment to unregistered attribute '#{key}'"
      end
    end
  end
end
