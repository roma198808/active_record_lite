require_relative 'db_connection'
require_relative '01_mass_object'
require 'active_support/inflector'

class MassObject
  def self.parse_all(results)
    all = []
    results.each do |hash|
      all << self.new(hash)
    end
    all
  end
end

class SQLObject < MassObject
  def self.table_name=(table_name)
    @table_name = table_name
  end

  def self.table_name
    @table_name ||= "cats"
  end

  def self.all
    results = DBConnection.execute <<-SQL
    SELECT *
    FROM #{table_name}
    SQL

    self.parse_all(results)
  end

  def self.find(id)
    results = DBConnection.execute <<-SQL
    SELECT *
    FROM #{table_name}
    WHERE #{table_name}.id = #{id}
    SQL

    self.new(results.first)
  end

  def insert
    col_names = self.class.attributes.join(',')
    question_marks = (["?"] * self.class.attributes.count).join(',')
    values = attribute_values

    DBConnection.execute(<<-SQL, values)
    INSERT INTO #{self.class.table_name} (#{col_names})
    VALUES (#{question_marks})
    SQL

    self.id = DBConnection.last_insert_row_id
  end

  def save
    self.id.nil? ? insert : update
  end

  def update
    set_line = self.class.attributes.map { |x| "#{x} = ?" }.join(',')

    result = DBConnection.execute(<<-SQL, *attribute_values)
    UPDATE #{self.class.table_name}
    SET #{set_line}
    WHERE #{self.class.table_name}.id = #{self.id}
    SQL

    p result
  end

  def attribute_values
    self.class.attributes.map { |x| self.send(x) }
  end
end
